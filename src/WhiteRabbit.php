<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        $handle = fopen($filePath, "r") or die("Unable to open file!");

        if ($handle) {
            $letterCountArray = array();
            while (!feof($handle)) {
                $buffer = fgets($handle, 4096);
                $letters = str_split($buffer);
                foreach ($letters as $letter) {
                    if (ctype_alpha($letter)) {
                        if(isset($letterCountArray[strtolower($letter)])){
                            $letterCountArray[strtolower($letter)]++;
                        }else{
                            $letterCountArray[strtolower($letter)] = 1;
                        }
                        
                    }
                }
            }
        }
        
        fclose($handle);
        arsort($letterCountArray);
        return $letterCountArray;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        if(is_array($parsedFile)){
            
            $arraySize=sizeof($parsedFile);
            $keysArray=array_keys($parsedFile);

            // Get median letter by dividing size of array by 2 and forcing it as int
            // Could be altered to take middle two in case of even number of letters
            // however would require change in main function above.
            $letter = $keysArray[intval($arraySize/2)];

            $occurrences = $parsedFile[$letter];
            return $letter;
        }

        
    }
}