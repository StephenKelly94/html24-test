<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
        $returnArray=array(
            '1'   => 0,
            '2'   => 0,
            '5'   => 0,
            '10'  => 0,
            '20'  => 0,
            '50'  => 0,
            '100' => 0
        );
        $keysArray=array_keys($returnArray);
        for ($i=sizeof($returnArray)-1; $i >= 0 ; $i--) { 
            $coin = $keysArray[$i];
            $coinCount = $this->subtractCoin($amount, $coin);
            $returnArray[$coin] = $coinCount;
            $amount = $amount - ($coinCount * (int)$coin);
        }
        return $returnArray;
    }

    /**
     * Function that takes in the total amount and coin size and recursively
     * sees how many coins of that amount it can take
     * 
     * @param $amount
     * @param $coin
     */
    public function subtractCoin($amount, $coin){
        if($amount - (int)$coin >= 0){
            $amount = $amount - (int)$coin;
            return $this->subtractCoin($amount, $coin) + 1;
        }else{
            return 0;
        }
    }
}